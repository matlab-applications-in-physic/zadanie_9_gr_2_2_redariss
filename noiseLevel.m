
%function searches through histogram data, to determine the most often occuring value of signal(noise)
function [noise] = noiseLevel(data)					

	[ indeks] = find(data == max(data));
	noise = indeks;
end
