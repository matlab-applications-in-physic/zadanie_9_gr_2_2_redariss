%{
author:Radoslaw Paluch

TechnicalPhysics
%}
%script with function should be in the same directory as data file

function [] = PeakAnalysis(var)
%'z3_1350V_x20.dat'

%openig file to analyse
fprintf('started, loading data...')
fileName=var;
file=dir(fileName);
fwd=fopen(fileName,'r');

%dividing the whole data set into 256 chunks to process

chopSize=33554432; 

%variable for data for histogram
dataSignal=zeros(1,255);

%reading from file by chunks and loading to histogram variavle

for i=1:fix(file.bytes/chopSize)
    dataTemp=fread(fwd,chopSize,'uint8');
    for k= 1:chopSize
        dataSignal(dataTemp(k))=dataSignal(dataTemp(k))+1;
    end
    
    
end

%clearing buffer
clear dataTemp; 
fclose(fwd);
fprintf('done\n');
fprintf('processing data may take 15 minutes\n');

%searching for noise level in signal
noiseInSignal = noiseLevel(dataSignal);
%reading file again to count pulses
fwd = fopen(fileName, 'r');
  pulseCount = 0;
  while ~feof(fwd)%idea for type of loop from Adam Stasiak
      
      dataTemp = fread(fwd, chopSize, 'uint8');
      %setting relative zero at noise level 
      dataTemp=dataTemp-noiseInSignal;
      %counting pules by CountPulses function
      pulseCount = pulseCount + CountPulses(dataTemp);
      clear dataTemp;
  end

  fclose(fwd);


%displaying results
fprintf('Noise level: %d \n', noiseInSignal);
fprintf('number of pulses: %d \n',pulseCount);
%plotting histogram
figure(1);
P = plot(dataSignal);
xlabel('values');
ylabel('amount');
title('Histogram');
%saving histogram in pdf format
name = [var, '_histogram.pdf'];
saveas(P, name);

%creating a file to store analysis results
fwd = fopen('z3_1350V_x20_peak_analysis_results.dat ','w');
  fprintf(fwd, 'Noise level: %d\n', noiseInSignal);
  fprintf(fwd, 'Number of pulses: %d\n', pulseCount);
  %from formula on uncertanity of counters
  pulseError = round(sqrt(pulseCount), 2, 'significant');
  fprintf(fwd, 'Pulse uncertainty: %.5f\n', pulseError);
  fclose(fwd);

end



