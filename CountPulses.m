function [pulses]=CountPulses(data)
%{
    author:Radoslaw Paluch

    TechnicalPhysics
%}
%about 31 742 219 pulses
pulses=0;
amount=0;
j=1;
while(j<fix(size(data,1)))
    
       %seting a treshold value of 5, above which we expect a peak
       if data(j)>5
           %if we have a candidate, calculating a gradient to get a
           %direction of change in value
           try
           grad=gradient(data(j:j+30));
           catch
               %if we exceed number of array elements(j+30) than we are at
               %the end of dataChunk and we stop counting
               break
           end
           grad=nonzeros(grad);
           grad=reshape(grad,[1,numel(grad)]);
           %checking if gradient changed sign, strfind 
           %searches str for occurrences of pattern (in this case change in
           %sing)
           amount=numel(strfind(sign(grad),[-1 1]));
           j=j+30;
           %count pulses
           pulses=pulses+amount;
       else
           j=j+1;
       
       end
end
end

